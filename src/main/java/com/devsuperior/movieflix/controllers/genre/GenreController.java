package com.devsuperior.movieflix.controllers.genre;

import com.devsuperior.movieflix.dto.GenreDTO;
import com.devsuperior.movieflix.services.genre.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/genres")
public class GenreController {

    private final GenreService genreService;

    @Autowired
    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }


    @GetMapping
    public ResponseEntity<List<GenreDTO>> findAll() {
        final var genres = genreService.findAll();
        return ResponseEntity.ok().body(genres);
    }
}
