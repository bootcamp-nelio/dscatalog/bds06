package com.devsuperior.movieflix.services;

import com.devsuperior.movieflix.dto.ReviewDTO;
import com.devsuperior.movieflix.entities.Review;
import com.devsuperior.movieflix.repositories.MovieRepository;
import com.devsuperior.movieflix.repositories.ReviewRepository;
import com.devsuperior.movieflix.repositories.UserRepository;
import com.devsuperior.movieflix.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewService {

    private final ReviewRepository repository;

    private final MovieRepository movieRepository;

    private final UserRepository userRepository;

    @Autowired
    public ReviewService(ReviewRepository repository, MovieRepository movieRepository, UserRepository userRepository) {
        this.repository = repository;
        this.movieRepository = movieRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public ReviewDTO insert(ReviewDTO reviewDTO) {
        var review = new Review();
        copyDtoToEntity(reviewDTO, review);
        review = repository.save(review);
        return new ReviewDTO(review);
    }

    private void copyDtoToEntity(ReviewDTO dto, Review entity) {
        final var movie = movieRepository.findById(dto.getMovie()).orElseThrow(() -> new ResourceNotFoundException("movie not found"));
        final var user = userRepository.findByEmail(getUser()).orElseThrow(() -> new ResourceNotFoundException("user not found"));
        entity.setId(dto.getId());
        entity.setMovie(movie);
        entity.setText(dto.getText());
        entity.setUser(user);
    }

    private String getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String nome;
        if (principal instanceof UserDetails) {
            nome = ((UserDetails)principal).getUsername();
        } else {
            nome = principal.toString();
        }
        return nome;
    }

    public List<ReviewDTO> findAll() {
        final var list = repository.findAll();
        return list.stream().map(ReviewDTO::new).collect(Collectors.toList());
    }
}
