package com.devsuperior.movieflix.services.movie;

import com.devsuperior.movieflix.dto.MovieDTO;
import com.devsuperior.movieflix.dto.ReviewDTO;
import com.devsuperior.movieflix.repositories.MovieRepository;
import com.devsuperior.movieflix.repositories.ReviewRepository;
import com.devsuperior.movieflix.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    private final ReviewRepository reviewRepository;

    @Autowired
    public MovieService(final MovieRepository movieRepository, final ReviewRepository reviewRepository) {
        this.movieRepository = movieRepository;
        this.reviewRepository = reviewRepository;
    }

    @Transactional(readOnly = true)
    public MovieDTO findById(final Long id) {
        final var movie =  movieRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Movie not found"));
        return new MovieDTO(movie);
    }

    @Transactional(readOnly = true)
    public List<ReviewDTO> findReviewsByMovieId(Long id) {
        return reviewRepository.findByMovie_Id(id).stream().map(ReviewDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Page<MovieDTO> find(Long genreId, Pageable pageable) {
        final var list = movieRepository.find(genreId, pageable);
        return list.map(MovieDTO::new);
    }

    @Transactional(readOnly = true)
    public Page<MovieDTO> findAllPaged() {
        final var pageable = PageRequest.of(0,25,Sort.by("title"));
        final var list = movieRepository.findAll(pageable);
        return list.map(MovieDTO::new);
    }
}
