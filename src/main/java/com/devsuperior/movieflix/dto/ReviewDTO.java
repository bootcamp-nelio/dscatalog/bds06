package com.devsuperior.movieflix.dto;

import com.devsuperior.movieflix.entities.Movie;
import com.devsuperior.movieflix.entities.Review;
import com.devsuperior.movieflix.entities.User;

import java.io.Serializable;

public class ReviewDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String text;
    private Long movieId;
    private UserDTO userDTO = new UserDTO(0l, "", "");

    public ReviewDTO() {
    }

    public ReviewDTO(Long id, String text, Movie movie, User user) {
        this.id = id;
        this.text = text;
        this.movieId = movie.getId();
        this.userDTO.setId(user.getId());
        this.userDTO.setName(user.getName());
        this.userDTO.setEmail(user.getEmail());
    }

    public ReviewDTO(Review review) {
        this.id = review.getId();
        this.text = review.getText();
        this.movieId = review.getMovie().getId();
        this.userDTO.setId(review.getUser().getId());
        this.userDTO.setName(review.getUser().getName());
        this.userDTO.setEmail(review.getUser().getEmail());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getMovie() {
        return movieId;
    }

    public void setMovie(Movie movie) {
        this.movieId = movie.getId();
    }

    public void setMovieId(Long id) {
        this.movieId = id;
    }

    public UserDTO getUser() {
        return userDTO;
    }

    public void setUser(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
