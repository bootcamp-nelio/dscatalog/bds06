package com.devsuperior.movieflix.dto;

import com.devsuperior.movieflix.entities.User;

public class UserUpdateDTO extends UserDTO {
    public UserUpdateDTO(User user) {
        super(user);
    }
}
