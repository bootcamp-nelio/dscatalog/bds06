create table genres (id  bigserial not null, name varchar(255), primary key (id));
create table movies (id  bigserial not null, img_url varchar(255), sub_title varchar(255), synopsis varchar(1000), title varchar(255), year int4, genre_id int8, primary key (id));
create table reviews (id  bigserial not null, text varchar(255), movie_id int8, user_id int8, primary key (id));
create table roles (id  bigserial not null, authority varchar(255), primary key (id));
create table user_roles (user_id int8 not null, role_id int8 not null, primary key (user_id, role_id));
create table users (id  bigserial not null, email varchar(255), name varchar(255), password varchar(255), primary key (id));
alter table movies add constraint FKjp8fsy8a0kkmdi04i81v05c6a foreign key (genre_id) references genres;
alter table reviews add constraint FK87tlqya0rq8ijfjscldpvvdyq foreign key (movie_id) references movies;
alter table reviews add constraint FKcgy7qjc1r99dp117y9en6lxye foreign key (user_id) references users;
alter table user_roles add constraint FKh8ciramu9cc9q3qcqiv4ue8a6 foreign key (role_id) references roles;
alter table user_roles add constraint FKhfh9dx7w3ubf1co1vdev94g3f foreign key (user_id) references users;

